package it.anleonard.myrecipes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyrecipesServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyrecipesServicesApplication.class, args);
	}

}
