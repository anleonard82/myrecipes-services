package it.anleonard.myrecipes.dao;

import it.anleonard.myrecipes.model.Recipe;

import java.util.List;
import java.util.UUID;

public interface RecipeDao {

    int insertNewRecipe(UUID recipeId, Recipe recipe);
    Recipe selectRecipeById(UUID recipeId);
    List<Recipe> selectAllRecipes();
    int updateRecipeById(UUID recipeId, Recipe newRecipe);
    int deleteRecipeById(UUID recipeId);


}
