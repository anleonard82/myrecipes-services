package it.anleonard.myrecipes.dao;

import it.anleonard.myrecipes.model.Recipe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@Repository
public class RecipeDaoImpl implements RecipeDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int insertNewRecipe(UUID recipeId, Recipe recipe) {
        return jdbcTemplate.update(
                "insert into recipes (id, name, description, imagePath) values(?,?,?,?)",
                "ciao", recipe.getName(), recipe.getDescription(), recipe.getImage_path());
    }

    @Override
    public Recipe selectRecipeById(UUID recipeId) {
        return null;
    }

    @Override
    public List<Recipe> selectAllRecipes() {
        return jdbcTemplate.query(
                "select * from recipes", new RecipeMapper());
    }

    @Override
    public int updateRecipeById(UUID recipeId, Recipe newRecipe) {
        return 1;
    }

    @Override
    public int deleteRecipeById(UUID recipeId) {
        return 1;
    }


    private static final class RecipeMapper implements RowMapper<Recipe> {

        public Recipe mapRow(ResultSet rs, int rowNum) throws SQLException {
            Recipe recipe = new Recipe();
            recipe.setName(rs.getString("name"));
            recipe.setDescription(rs.getString("description"));
            recipe.setImage_path(rs.getString("imagePath"));
            return recipe;
        }
    }
}
