package it.anleonard.myrecipes.service;

import com.sun.xml.internal.bind.v2.model.core.ID;
import it.anleonard.myrecipes.dao.RecipeDao;
import it.anleonard.myrecipes.model.Recipe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import javax.persistence.Id;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class RecipeService {

    @Autowired
    private RecipeRepository recipeRepository;

    private RecipeRepositoryMapper mapper;

    @Autowired
    public RecipeService() {  }

    public int persistNewRecipe(long recipeId, Recipe recipe){
       return 0;
    }
    public Recipe getRecipeById(long recipeId){
        return null;
    }

    public List<Recipe> getAllRecipes(){
        return mapper.convert(recipeRepository.findAll());
    }

    public void updateRecipeById(Recipe newRecipe){
        recipeRepository.save(mapper.convert(newRecipe));
    }

    public void deleteRecipeById(long recipeId){

    }

    private static final class RecipeRepositoryMapper {

        public static Recipe convert(it.anleonard.myrecipes.repository.Recipe r){
            Recipe recipe = new Recipe();
            recipe.setId(r.getId());
            recipe.setName(r.getName());
            recipe.setDescription(r.getDescription());
            recipe.setImage_path(r.getImage_path());
            return recipe;
        }

        public static it.anleonard.myrecipes.repository.Recipe convert(Recipe r){
            it.anleonard.myrecipes.repository.Recipe recipe = new it.anleonard.myrecipes.repository.Recipe();
            recipe.setId(r.getId());
            recipe.setName(r.getName());
            recipe.setDescription(r.getDescription());
            recipe.setImage_path(r.getImage_path());
            return recipe;
        }

        public static List<Recipe> convert(Iterable<it.anleonard.myrecipes.repository.Recipe> iterable){
            if(iterable==null) return null;
            List<Recipe> recipes = new ArrayList<Recipe>();
            for (it.anleonard.myrecipes.repository.Recipe r : iterable) {
                recipes.add(convert(r));
            }
            return recipes;
        }
    }
}
