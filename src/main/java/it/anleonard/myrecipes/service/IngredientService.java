package it.anleonard.myrecipes.service;

import it.anleonard.myrecipes.model.Recipe;
import it.anleonard.myrecipes.repository.Ingredient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class IngredientService {

    @Autowired
    private IngredientRepository ingredientRepository;

    private IngredientRepositoryMapper mapper;

    @Autowired
    public IngredientService() {  }

    public int persistNewIngredient(long ingredientId, Ingredient ingredient){
       return 0;
    }
    public Ingredient geIngredientById(long ingredientId){
        return null;
    }
    public List<it.anleonard.myrecipes.model.Ingredient> getAllIngredients(){
        return mapper.convert(ingredientRepository.findAll());
    }
    public void updateIngredientById(it.anleonard.myrecipes.model.Ingredient ingredient){
        ingredientRepository.save(mapper.convert(ingredient));
    }
    public void deleteIngredientById(long ingredientId){  }

    private static final class IngredientRepositoryMapper {

        public static it.anleonard.myrecipes.model.Ingredient convert(it.anleonard.myrecipes.repository.Ingredient i){
            it.anleonard.myrecipes.model.Ingredient ingredient = new it.anleonard.myrecipes.model.Ingredient();
            ingredient.setId(i.getId());
            ingredient.setName(i.getName());
            ingredient.setDescription(i.getDescription());
            ingredient.setUnit(i.getUnit());
            return ingredient;
        }

        public static it.anleonard.myrecipes.repository.Ingredient convert(it.anleonard.myrecipes.model.Ingredient r){
            it.anleonard.myrecipes.repository.Ingredient ingredient = new it.anleonard.myrecipes.repository.Ingredient();
            ingredient.setId(r.getId());
            ingredient.setName(r.getName());
            ingredient.setDescription(r.getDescription());
            ingredient.setUnit(r.getUnit());
            return ingredient;
        }

        public static List<it.anleonard.myrecipes.model.Ingredient> convert(Iterable<it.anleonard.myrecipes.repository.Ingredient> iterable){
            if(iterable==null) return null;
            List<it.anleonard.myrecipes.model.Ingredient> ingredients = new ArrayList<it.anleonard.myrecipes.model.Ingredient>();
            for (it.anleonard.myrecipes.repository.Ingredient i : iterable) {
                ingredients.add(convert(i));
            }
            return ingredients;
        }
    }
}
