package it.anleonard.myrecipes.service;

import it.anleonard.myrecipes.repository.Ingredient;
import it.anleonard.myrecipes.repository.Recipe;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IngredientRepository extends CrudRepository<Ingredient, Long> {

}
