package it.anleonard.myrecipes.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

/**
 * Recipe
 */
public class Recipe {

    private long id;
    private String name;
    private String description;
    private String image_path;

    public Recipe(
            @JsonProperty("id") long id,
            @JsonProperty("name") String name,
            @JsonProperty("description") String description,
            @JsonProperty("imagePath") String imagePath) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.image_path = imagePath;
    }

    public Recipe() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public String getDescription() {
        return description;
    }

    public String getImage_path() {
        return image_path;
    }


}
