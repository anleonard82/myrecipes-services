package it.anleonard.myrecipes.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Ingredient
 */
public class Ingredient {

    private long id;
    private String name;
    private String description;
    private String unit;

    public Ingredient(
            @JsonProperty("id") long id,
            @JsonProperty("name") String name,
            @JsonProperty("description") String description,
            @JsonProperty("unit") String unit) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.unit = unit;
    }

    public Ingredient() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getDescription() {
        return description;
    }

    public String getUnit() {
        return unit;
    }


}
