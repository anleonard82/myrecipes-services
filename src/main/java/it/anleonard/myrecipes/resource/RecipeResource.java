package it.anleonard.myrecipes.resource;

import it.anleonard.myrecipes.model.Recipe;
import it.anleonard.myrecipes.service.RecipeService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/recipes")
@CrossOrigin(origins = "*")
public class RecipeResource {

    public final RecipeService recipeService;

    public RecipeResource(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public List<Recipe> getAllRecipes(){
        return recipeService.getAllRecipes();
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            path = "{recipeId}"
    )
    public Recipe getRecipeById(@PathVariable("recipeId") UUID recipeId){
        return null;
        //return recipeService.getRecipeById(recipeId);
    }

    @RequestMapping(
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public int insertRecipe(@RequestBody Recipe recipe){
        return recipeService.persistNewRecipe(1, recipe);
    }

    @RequestMapping(
            method = RequestMethod.DELETE,
            path = "{recipeId}"
    )
    public void deleteRecipe(@PathVariable("recipeId") long recipeId){

    }

    @RequestMapping(
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public int updateRecipe(@RequestBody Recipe recipe){
       return 0;
       // return recipeService.updateRecipeById(1, recipe);
    }

}
